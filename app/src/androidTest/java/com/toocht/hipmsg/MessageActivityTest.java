package com.toocht.hipmsg;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.toocht.hipmsg.json.JsonSerializable;
import com.toocht.hipmsg.model.Link;
import com.toocht.hipmsg.model.MessageResult;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

/**
 * Created by tool on 2017-01-27.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MessageActivityTest {

    private static String NBC_OLYMPICS_TITLE = "2018 PyeongChang Olympic Games | NBC Olympics";
    private static String TWITTER_JDORFMAN_TITLE = "Justin Dorfman on Twitter: nice @littlebigdetai...";

    private static String NBC_OLYMPICS_FILE_NAME = "nbc_olympics.html";
    private static String TWITTER_JDORFMAN_FILE_NAME = "twitter_jdorfman.html";
    private static String NO_TITLE_FILE_NAME = "no_title.html";
    private static String ONLY_OG_TITLE_FILE_NAME = "only_og_title.html";

    private MessageRobot robot;
    private MockWebServer server;

    @Rule
    public ActivityTestRule<MessageActivity> mActivityRule = new ActivityTestRule<>(
            MessageActivity.class);

    @Before
    public void initRobot() {
        robot = new MessageRobot();
        server = new MockWebServer();
    }

    @Test
    public void testSimpleMessage() {
        // Set up the expected result.
        MessageResult expectedResult = new MessageResult(null, null, null);

        robot.typeMessage("This is a simple message: Hi there, I hope you have a nice day!").
                pressSendButton().
                checkResult(getJson(expectedResult));
    }

    @Test
    public void testOneMention() {
        // Set up the expected result.
        List<String> mentions = Arrays.asList(new String[] {"chris"});
        MessageResult expectedResult = new MessageResult(mentions, null, null);

        robot.typeMessage("@chris you around?").
                pressSendButton().
                checkResult(getJson(expectedResult));
    }

    @Test
    public void testManyMentions() {
        // Set up the expected result.
        List<String> mentions = Arrays.asList(new String[] {"chris", "andy", "anna", "maria", "chuck", "klaus"});
        MessageResult expectedResult = new MessageResult(mentions, null, null);

        robot.typeMessage("Many mentions: @chris 123 @andy 432 @anna ads @maria @chuck @klaus").
                pressSendButton().
                checkResult(getJson(expectedResult));
    }

    @Test
    public void testInvalidMentions() {
        // Set up the expected result.
        MessageResult expectedResult = new MessageResult(null, null, null);

        robot.typeMessage("Invalid mentions: @@ @_ @:").
                pressSendButton().
                checkResult(getJson(expectedResult));
    }

    @Test
    public void testTwoEmoticons() {
        // Set up the expected result.
        List<String> emoticons = Arrays.asList(new String[] {"megusta", "coffee"});
        MessageResult expectedResult = new MessageResult(null, emoticons, null);

        robot.typeMessage("Good morning! (megusta) (coffee)").
                pressSendButton().
                checkResult(getJson(expectedResult));
    }

    @Test
    public void testInvalidEmoticons() {
        // Set up the expected result.
        MessageResult expectedResult = new MessageResult(null, null, null);

        robot.typeMessage("Invalid emoticons: Good morning! (123 asdf) ( ) (asd*123) (asd/asd)").
                pressSendButton().
                checkResult(getJson(expectedResult));
    }

    @Test
    public void testOneLink() throws Exception {
        String url = startMockServer(readFile(NBC_OLYMPICS_FILE_NAME));

        // Set up the expected result.
        List<Link> links = Arrays.asList(new Link[] {new Link(NBC_OLYMPICS_TITLE, url)});
        MessageResult expectedResult = new MessageResult(null, null, links);

        robot.typeMessage("Olympics are starting soon; " + url).
                pressSendButton().
                checkResult(getJson(expectedResult));

        server.shutdown();
    }


    @Test
    public void testCutLinkAndRemoveQuoteTitle() throws Exception {
        String url = startMockServer(readFile(TWITTER_JDORFMAN_FILE_NAME));

        // Set up the expected result.
        List<String> mentions = Arrays.asList(new String[] {"bob", "john"});
        List<String> emoticons = Arrays.asList(new String[] {"success"});
        List<Link> links = Arrays.asList(new Link[] {new Link(TWITTER_JDORFMAN_TITLE, url)});
        MessageResult expectedResult = new MessageResult(mentions, emoticons, links);

        robot.typeMessage("@bob @john (success) such a cool feature; " + url).
                pressSendButton().
                checkResult(getJson(expectedResult));

        server.shutdown();
    }

    @Test
    public void testLinkWithNoTitle() throws Exception {
        String url = startMockServer(readFile(NO_TITLE_FILE_NAME));

        // Set up the expected result.
        List<Link> links = Arrays.asList(new Link[] {new Link(null, url)});
        MessageResult expectedResult = new MessageResult(null, null, links);

        robot.typeMessage("Link without title: " + url).
                pressSendButton().
                checkResult(getJson(expectedResult));

        server.shutdown();
    }

    @Test
    public void testLinkWithOnlyOGTitle() throws Exception {
        String url = startMockServer(readFile(ONLY_OG_TITLE_FILE_NAME));

        // Set up the expected result.
        List<Link> links = Arrays.asList(new Link[] {new Link("The Open Graph title", url)});
        MessageResult expectedResult = new MessageResult(null, null, links);

        robot.typeMessage("Link with only the Open Graph title: " + url).
                pressSendButton().
                checkResult(getJson(expectedResult));

        server.shutdown();
    }

    @Test
    public void testManyLinks() throws Exception {
        String nbcOlympicsHtml = readFile(NBC_OLYMPICS_FILE_NAME);
        String twitterJdorfmanHtml = readFile(TWITTER_JDORFMAN_FILE_NAME);
        String url = startMockServer(nbcOlympicsHtml,
                twitterJdorfmanHtml,
                nbcOlympicsHtml,
                twitterJdorfmanHtml,
                nbcOlympicsHtml,
                twitterJdorfmanHtml,
                nbcOlympicsHtml,
                twitterJdorfmanHtml);

        Link nbcOlympicsLink = new Link(NBC_OLYMPICS_TITLE, url);
        Link twitterJdorfmanLink = new Link(TWITTER_JDORFMAN_TITLE, url);

        // Set up the expected result.
        List<Link> links = Arrays.asList(new Link[] { nbcOlympicsLink,
                twitterJdorfmanLink,
                nbcOlympicsLink,
                twitterJdorfmanLink,
                nbcOlympicsLink,
                twitterJdorfmanLink,
                nbcOlympicsLink,
                twitterJdorfmanLink });
        MessageResult expectedResult = new MessageResult(null, null, links);

        String message = "Many links: ";
        // Append the links to the message.
        for (Link link : links) {
            message += url + " ";
        }

        robot.typeMessage(message).
                pressSendButton().
                checkResult(getJson(expectedResult));

        server.shutdown();
    }

    @Test
    public void testConnectionLinkTimeout() throws Exception {
        String url = startMockServer();
        server.enqueue(new MockResponse().setBodyDelay(8, TimeUnit.SECONDS).setBody(readFile(TWITTER_JDORFMAN_FILE_NAME)));

        // Set up the expected result.
        List<Link> links = Arrays.asList(new Link[] {new Link(null, url)});
        MessageResult expectedResult = new MessageResult(null, null, links);

        robot.typeMessage("Connection timeout, please wait a couple of seconds; " + url).
                pressSendButton().
                checkResult(getJson(expectedResult));

        server.shutdown();
    }

    @Test
    public void testConnectionLinkMovedPerm() throws Exception {
        String url = startMockServer();
        server.enqueue(new MockResponse().setResponseCode(HttpURLConnection.HTTP_MOVED_PERM).setHeader("Location", url));
        server.enqueue(new MockResponse().setBody(readFile(TWITTER_JDORFMAN_FILE_NAME)));

        // Set up the expected result.
        List<Link> links = Arrays.asList(new Link[] {new Link(TWITTER_JDORFMAN_TITLE, url)});
        MessageResult expectedResult = new MessageResult(null, null, links);

        robot.typeMessage("Link permanently moved: " + url).
                pressSendButton().
                checkResult(getJson(expectedResult));

        server.shutdown();
    }

    @Test
    public void testConnectionLinkMovedTemp() throws Exception {
        String url = startMockServer();
        server.enqueue(new MockResponse().setResponseCode(HttpURLConnection.HTTP_MOVED_TEMP).setHeader("Location", url));
        server.enqueue(new MockResponse().setBody(readFile(TWITTER_JDORFMAN_FILE_NAME)));

        // Set up the expected result.
        List<Link> links = Arrays.asList(new Link[] {new Link(TWITTER_JDORFMAN_TITLE, url)});
        MessageResult expectedResult = new MessageResult(null, null, links);

        robot.typeMessage("Link temporary moved: " + url).
                pressSendButton().
                checkResult(getJson(expectedResult));

        server.shutdown();
    }

    @Test
    public void testConnectionLinkMovedServerError() throws Exception {
        MockResponse response = new MockResponse().setResponseCode(HttpURLConnection.HTTP_INTERNAL_ERROR);
        String url = startMockServer(Arrays.asList(new MockResponse[] { response }));

        // Set up the expected result.
        List<Link> links = Arrays.asList(new Link[] {new Link(null, url)});
        MessageResult expectedResult = new MessageResult(null, null, links);

        robot.typeMessage("Server responds with internal server error (http code 500): " + url).
                pressSendButton().
                checkResult(getJson(expectedResult));

        server.shutdown();
    }


    // Helper methods.

    private String readFile(String path) throws IOException {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();

        try {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(path);
            reader = new BufferedReader(new InputStreamReader(in));
            String line = null;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }

        return sb.toString();
    }

    private String getJson(JsonSerializable jsonObject) {
        int indentSpaces = mActivityRule.getActivity().getResources().getInteger(R.integer.json_serialization_indent_spaces);

        try {
            String jsonString = jsonObject.toJSONObject().toString(indentSpaces);
            if (jsonString != null) {
                // Remove escape character for the slash.
                return jsonString.replace("\\/", "/");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String startMockServer(String... mockBodies) throws IOException {
        List<MockResponse> mockResponses = new ArrayList<>();
        for (String mockBody : mockBodies) {
            mockResponses.add(new MockResponse().setBody(mockBody));
        }
        return startMockServer(mockResponses);
    }

    private String startMockServer(List<MockResponse> mockResponses) throws IOException {
        for (MockResponse response : mockResponses) {
            server.enqueue(response);
        }
        server.start();

        // MockWebServer returns it's URL as http://localhost:<port>. The built in Android pattern
        // for matching URLs doesn't match against localhost, but the local ip (127.0.0.1) is ok.
        // So get the URL and replace localhost -> 127.0.0.1
        HttpUrl httpUrl = server.url("/the_rest_of_the_url");
        return httpUrl.toString().replace("localhost", "127.0.0.1");
    }


    private class MessageRobot {
        public MessageRobot typeMessage(String message) {
            Espresso.onView(ViewMatchers.withId(R.id.enter_message_edit_text))
                    .perform(ViewActions.typeText(message));
            Espresso.closeSoftKeyboard();
            return this;
        }

        public MessageRobot pressSendButton() {
            Espresso.onView(ViewMatchers.withId(R.id.send_message_button)).perform(ViewActions.click());
            return this;
        }

        public MessageRobot checkResult(String expectedResult) {
            Espresso.onView(ViewMatchers.withId(R.id.message_result_text_view))
                    .check(ViewAssertions.matches(ViewMatchers.withText(expectedResult)));
            return this;
        }
    }
}
