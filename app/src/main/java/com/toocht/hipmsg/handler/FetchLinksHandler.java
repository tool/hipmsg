package com.toocht.hipmsg.handler;

import android.content.Context;
import android.os.AsyncTask;

import com.toocht.hipmsg.R;
import com.toocht.hipmsg.model.Link;
import com.toocht.hipmsg.util.HtmlTitleMatcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tool on 2017-01-25.
 */

public class FetchLinksHandler {

    private List<String> linkUrls;
    private List<Link> links;
    private int completedFetchesCount;
    private FetchLinksListener listener;
    private List<FetchLinkAsyncTask> tasks;

    /**
     * Retrieves a list of HTML titles from the provided link URLs on a background thread. Calls the
     * listener when all the responses have been handled.
     * @param context the context.
     * @param linkUrls the list of URLs.
     * @param listener the listener.
     */
    public void fetchLinks(Context context, List<String> linkUrls, FetchLinksListener listener) {
        this.linkUrls = linkUrls;
        this.listener = listener;

        for (String linkUrl : linkUrls) {
            // Apparently Twitter changes the title of the page (probably) depending on if it the device
            // is handheld or not. The exact same result as the specification was gained with this user agent.
            // The user agent is Chrome 55.0.2883.95 in MacOS 10.12.2.
            new FetchLinkAsyncTask(context,
                    context.getResources().getString(R.string.fetch_link_title_user_agent)).execute(linkUrl);
        }
    }

    public void cancel() {
        if (tasks == null) {
            return;
        }

        for (FetchLinkAsyncTask task : tasks) {
            task.cancel(true);
        }
    }


    public interface FetchLinksListener {
        /**
         * Triggered when all the link titles have been fetched.
         * @param links
         */
        public void onLinksFetched(List<Link> links);
    }


    private class FetchLinkAsyncTask extends AsyncTask<String, Void, Link> {

        private HtmlTitleMatcher htmlTitleMatcher;
        private Context context;
        private String userAgent;
        private HttpURLConnection connection;

        public FetchLinkAsyncTask(Context context) {
            this(context, null);
        }

        public FetchLinkAsyncTask(Context context, String userAgent) {
            this.context = context;
            this.userAgent = userAgent;
            this.htmlTitleMatcher = new HtmlTitleMatcher();
        }

        @Override
        protected Link doInBackground(String... strings) {
            if (isCancelled() || strings == null || strings.length < 1 || strings[0] == null) {
                return null;
            }

            String url = strings[0];
            String html = fetchResponse(url);
            String title = null;
            if (html != null) {
                title = htmlTitleMatcher.findTitle(context, html);

                if (title != null) {
                    title = title.trim();
                }
            }

            return new Link(title, url);
        }

        @Override
        protected void onPostExecute(Link link) {
            if (isCancelled()) {
                return;
            }

            if (links == null) {
                links = new ArrayList<>();
            }

            links.add(link);

            // Notify the listener when all the link titles have been fetched.
            if (++completedFetchesCount == linkUrls.size() && listener != null) {
                listener.onLinksFetched(links);
            }
        }


        /**
         * Retrieves the response from an URL request. Follows redirects if needed.
         * @param urlString the URL string.
         * @return the response of the request.
         */
        private String fetchResponse(String urlString) {
            if (isCancelled()) {
                return null;
            }

            BufferedReader reader = null;

            try {
                URL url = new URL(urlString);
                connection = (HttpURLConnection) url.openConnection();

                // Set the user agent, if requested.
                if (userAgent != null) {
                    connection.setRequestProperty("User-Agent", userAgent);
                }

                // Set the timout of the connection.
                connection.setConnectTimeout(context.getResources().getInteger(R.integer.default_connection_timeout));
                connection.setReadTimeout(context.getResources().getInteger(R.integer.default_connection_read_timeout));


                int statusCode = connection.getResponseCode();

                // Read the response if the satus code is 200..299.
                if (statusCode >= HttpURLConnection.HTTP_OK &&
                        statusCode < HttpURLConnection.HTTP_MULT_CHOICE) {
                    reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line = null;
                    StringBuilder html = new StringBuilder();
                    while ((line = reader.readLine()) != null) {
                        html.append(line);
                    }
                    return html.toString();
                }
                // Follow the redirect if the status code is 301 or 302.
                else if (statusCode == HttpURLConnection.HTTP_MOVED_PERM ||
                        statusCode == HttpURLConnection.HTTP_MOVED_TEMP) {

                    // Follow the redirect.
                    final String redirectUrl = connection.getHeaderField("Location");
                    return fetchResponse(redirectUrl);
                }
            } catch (MalformedURLException e) {
                // Take care of this later. There will be no title in the link.
            } catch (IOException e) {
                // Take care of this later. There will be no title in the link.
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (connection != null) {
                    connection.disconnect();
                }
            }

            return null;
        }
    }
}
