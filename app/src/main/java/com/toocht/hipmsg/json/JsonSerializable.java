package com.toocht.hipmsg.json;

import org.json.JSONObject;

/**
 * Created by tool on 2017-01-21.
 */

public interface JsonSerializable {

    /**
     * Converts the object into a JSONObject.
     * @return the JSONObject representation.
     */
    public JSONObject toJSONObject();

}
