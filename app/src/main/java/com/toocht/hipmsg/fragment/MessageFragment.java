package com.toocht.hipmsg.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.toocht.hipmsg.R;

/**
 * Created by tool on 2017-01-18.
 */

public class MessageFragment extends Fragment {

    private static final String BUNDLE_KEY_MESSAGE_RESULT = "message_result";

    private TextView messageResultTextView;
    private EditText messageEditText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        messageResultTextView = (TextView) view.findViewById(R.id.message_result_text_view);
        messageEditText = (EditText) view.findViewById(R.id.enter_message_edit_text);
        handleSavedInstanceState(savedInstanceState);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_KEY_MESSAGE_RESULT, messageResultTextView.getText().toString());
    }

    private void handleSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            setResultMessage(savedInstanceState.getString(BUNDLE_KEY_MESSAGE_RESULT));
        }
    }

    /**
     * Retrieves the current input message.
     * @return the current input message.
     */
    public String getCurrentMessage() {
        return messageEditText.getText().toString();
    }

    /**
     * Displays the result message.
     * @param resultMessage the message to display.
     */
    public void setResultMessage(String resultMessage) {
        messageResultTextView.setText(resultMessage);
    }
}
