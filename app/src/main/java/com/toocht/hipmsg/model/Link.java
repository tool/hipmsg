package com.toocht.hipmsg.model;

import com.toocht.hipmsg.json.JsonSerializable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tool on 2017-01-18.
 */

public class Link implements JsonSerializable {

    private static final String JSON_KEY_TITLE = "title";
    private static final String JSON_KEY_URL = "url";

    private String title;
    private String url;

    public Link(String title, String url) {
        this.title = title;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Link{" +
                "title='" + title + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject jsonObject= new JSONObject();
        try {
            if (title != null && !title.trim().isEmpty()) {
                jsonObject.put(JSON_KEY_TITLE, title);
            }

            if (url != null && !url.trim().isEmpty()) {
                jsonObject.put(JSON_KEY_URL, url);
            }

            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
