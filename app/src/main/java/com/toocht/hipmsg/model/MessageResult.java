package com.toocht.hipmsg.model;

import com.toocht.hipmsg.json.JsonSerializable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by tool on 2017-01-21.
 */

public class MessageResult implements JsonSerializable {

    private static final String JSON_KEY_MENTIONS = "mentions";
    private static final String JSON_KEY_EMOTICONS = "emoticons";
    private static final String JSON_KEY_LINKS = "links";

    private List<String> mentions;
    private List<String> emoticons;
    private List<Link> links;

    public MessageResult(List<String> mentions, List<String> emoticons, List<Link> links) {
        this.mentions = mentions;
        this.emoticons = emoticons;
        this.links = links;
    }

    public List<String> getMentions() {
        return mentions;
    }

    public void setMentions(List<String> mentions) {
        this.mentions = mentions;
    }

    public List<String> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(List<String> emoticons) {
        this.emoticons = emoticons;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @Override
    public JSONObject toJSONObject(){
        JSONObject jsonObject= new JSONObject();
        try {
            if (mentions != null && !mentions.isEmpty()) {
                jsonObject.put(JSON_KEY_MENTIONS, getJsonStringArray(mentions));
            }

            if (emoticons != null && !emoticons.isEmpty()) {
                jsonObject.put(JSON_KEY_EMOTICONS, getJsonStringArray(emoticons));
            }

            if (links != null && !links.isEmpty()) {
                jsonObject.put(JSON_KEY_LINKS, getJsonSerializableArray(links));
            }

            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private JSONArray getJsonStringArray(List<String> strings) {
        JSONArray jsonArray = new JSONArray();
        for (String string : strings) {
            jsonArray.put(string);
        }
        return jsonArray;
    }

    private JSONArray getJsonSerializableArray(List<? extends JsonSerializable> array) {
        JSONArray jsonArray = new JSONArray();
        for (JsonSerializable object : array) {
            jsonArray.put(object.toJSONObject());
        }
        return jsonArray;
    }
}
