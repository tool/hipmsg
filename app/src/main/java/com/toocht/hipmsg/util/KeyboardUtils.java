package com.toocht.hipmsg.util;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by tool on 2017-01-26.
 */

public class KeyboardUtils {

    /**
     * Hides the soft keyboard.
     * @param activity the current activity.
     */
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputManager =
                (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        IBinder binder = activity.getCurrentFocus() == null ?
                null : activity.getCurrentFocus().getWindowToken();

        inputManager.hideSoftInputFromWindow(binder, InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
