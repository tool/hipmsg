package com.toocht.hipmsg.util;

import android.content.Context;
import android.support.v4.util.PatternsCompat;

import com.toocht.hipmsg.R;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by tool on 2017-01-18.
 */

public class MessageMatcher extends BaseMatcher {

    private static final String TAG = "MessageMatcher";

    private static final String ALPHA_NUMERIC_REGEX = "[a-zA-Z0-9]";

    private static Pattern mentionPattern = Pattern.compile("@(" + ALPHA_NUMERIC_REGEX + "+)");
    private static Pattern emoticonPattern = Pattern.compile("\\((" + ALPHA_NUMERIC_REGEX + "{1,15})\\)");
    private static Pattern urlProtocolPattern = Pattern.compile("^(http|https|rtsp)");


    /**
     * Retrieves mentions from a string. A mention is preceeded by a @.
     * @param searchString the string to search.
     * @return a list of mentions.
     */
    public List<String> findMentions(String searchString) {
        return findMatches(mentionPattern, searchString);
    }

    /**
     * Retrieves emoticons from a string. An emiticon is surrounded by parenthesis.
     * @param searchString the string to search.
     * @return a list of emoticons.
     */
    public List<String> findEmoticons(String searchString) {
        return findMatches(emoticonPattern, searchString);
    }

    /**
     * Retrieves URLs from a string.
     * @param context the context.
     * @param searchString the string to search.
     * @return a list of URLs. The default protocol is added (https) if no protocol is defined.
     */
    public List<String> findLinkUrls(Context context, final String searchString) {
        List<String> tmpLinkUrls = findMatches(PatternsCompat.WEB_URL, searchString);

        // Append the default protocol if the url doesn't contain a protocol.
        List<String> linkUrls = new ArrayList<>();

        for (String linkUrl : tmpLinkUrls) {
            if (hasProtocol(linkUrl)) {
                linkUrls.add(linkUrl);
            } else {
                linkUrls.add(context.getString(R.string.default_link_protocol) + linkUrl);
            }
        }

        return linkUrls;
    }

    /**
     * Checks if the URL contains a protocol.
     * @param linkUrl the URL to search.
     * @return true if a protocol is found, falase otherwise.
     */
    private boolean hasProtocol(String linkUrl) {
        return urlProtocolPattern.matcher(linkUrl).find();
    }

}
