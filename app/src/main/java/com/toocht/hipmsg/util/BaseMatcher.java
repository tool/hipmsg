package com.toocht.hipmsg.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tool on 2017-01-22.
 */

public class BaseMatcher {

    /**
     * Retrieves a list of groups from a regex that matches the search string.
     * @param pattern the regex pattern.
     * @param searchString the search string.
     * @return a list of matching groups. Returns an empty list if no match was found.
     */
    protected List<String> findMatches(Pattern pattern, String searchString) {
        Matcher m = pattern.matcher(searchString);
        List<String> results = new ArrayList<>();

        while (m.find()) {
            results.add(m.group(1));
        }

        return results;
    }

    /**
     * Retrieves the first groups from a regex that matches the search string.
     * @param pattern the regex pattern.
     * @param searchString the search string.
     * @return the first matching group. Returns null if no match was found.
     */
    protected String findFirstMatch(Pattern pattern, String searchString) {
        Matcher m = pattern.matcher(searchString);
        String result = null;

        if (m.find()) {
            return m.group(1);
        }

        return null;
    }

}
