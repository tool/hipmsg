package com.toocht.hipmsg.util;

/**
 * Created by tool on 2017-01-26.
 */

public class TextUtils {

    /**
     * Cuts the string and adds the ellipsis if the string is longer than the max length.
     * @param s the string to manipulate.
     * @param maxLength the maximum length of the result, including the ellipsis.
     * @param ellipsis the string to add at the end of the input string - if it is cut.
     * @return the manipulated string.
     */
    public String ellipsize(String s, int maxLength, String ellipsis) {
        if (s == null || s.length() <= maxLength) {
            return s;
        }
        return s.substring(0, maxLength - ellipsis.length()).concat(ellipsis);
    }

}
