package com.toocht.hipmsg.util;

import android.content.Context;
import android.os.Build;
import android.text.Html;

import com.toocht.hipmsg.R;

import java.util.regex.Pattern;

/**
 * Created by tool on 2017-01-22.
 */

public class HtmlTitleMatcher extends BaseMatcher {

    private static final String TAG = "HtmlTitleMatcher";

    private static Pattern ogTitlePattern = Pattern.compile("<meta\\s*property=[\"']og:title[\"']\\s*content=[\"'](.*?)[\"']");
    private static Pattern titlePattern = Pattern.compile("<title>(.*?)</title>");

    private TextUtils textUtils = new TextUtils();

    /**
     * Retrieves the value of the HTML title tag and falls back to the og:title meta tag.
     * @param context the context.
     * @param html the HTML string to search.
     * @return the value of the HTML title tag if found otherwise the og:title meta tag. If none of
     * the tag values are found null is returned.
     */
    public String findTitle(Context context, String html) {
        // Extract the title tag.
        String title = findFirstMatch(titlePattern, html);

        if (title != null && !title.trim().isEmpty()) {
            return clean(context, title);
        }

        // Fall back to the Open Graph title.
        title = findFirstMatch(ogTitlePattern, html);

        if (title != null && !title.trim().isEmpty()) {
            return clean(context, title);
        }

        return null;
    }


    /**
     * Cleans the html by decoding, removing quotes and ellipsize-ing the string.
     * @param context the context.
     * @param s the string to clean.
     * @return the cleaned string.
     */
    private String clean(Context context, String s) {
        String title = new String(s);

        title = decodeHtml(title);

        if (title != null) {
            // Replace quotes.
            title = title.replace("\"", "");

            // Ellipsize.
            title = textUtils.ellipsize(title,
                    context.getResources().getInteger(R.integer.link_title_max_length),
                    context.getString(R.string.ellipsis));
        }

        return title;
    }

    @SuppressWarnings("deprecation")
    private String decodeHtml(String encodedHtml) {
        String decodedHtml = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            decodedHtml = Html.fromHtml(encodedHtml, 0).toString();
        } else {
            decodedHtml = Html.fromHtml(encodedHtml).toString();
        }

        return decodedHtml;
    }
}
