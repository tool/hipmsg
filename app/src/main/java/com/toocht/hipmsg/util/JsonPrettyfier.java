package com.toocht.hipmsg.util;

import android.content.Context;

import com.toocht.hipmsg.R;
import com.toocht.hipmsg.json.JsonSerializable;

import org.json.JSONException;

/**
 * Created by tool on 2017-01-26.
 */

public class JsonPrettyfier {

    /**
     * Converts the JSON serializable object into a JSON string that is made readable by adding
     * indents and removing escaped slashes.
     * @param context the context.
     * @param jsonObject the JSON serializable object.
     * @return the readable JSON string.
     */
    public String toReadableString(Context context, JsonSerializable jsonObject) {
        try {
            int indentSpaces = context.getResources().getInteger(R.integer.json_serialization_indent_spaces);
            String jsonString = jsonObject.toJSONObject().toString(indentSpaces);

            if (jsonString != null) {
                // Remove escape character for the slash.
                return jsonString.replace("\\/", "/");
            }
        } catch (JSONException e) {
            // Return null to handle the error.
        }
        return null;
    }

}
