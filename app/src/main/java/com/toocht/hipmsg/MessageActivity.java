package com.toocht.hipmsg;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.toocht.hipmsg.fragment.MessageFragment;
import com.toocht.hipmsg.handler.FetchLinksHandler;
import com.toocht.hipmsg.model.Link;
import com.toocht.hipmsg.model.MessageResult;
import com.toocht.hipmsg.util.JsonPrettyfier;
import com.toocht.hipmsg.util.KeyboardUtils;
import com.toocht.hipmsg.util.MessageMatcher;

import java.util.List;

public class MessageActivity extends AppCompatActivity implements FetchLinksHandler.FetchLinksListener {

    private static final String TAG = "MessageActivity";

    private static final String BUNDLE_KEY_PROGRESS_DIALOG_SHOWING = "progress_dialog_showing";

    private MessageMatcher matcher;

    private MessageFragment messageFragment;

    private FetchLinksHandler fetchLinksHandler;

    private JsonPrettyfier jsonPrettyfier;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        matcher = new MessageMatcher();
        jsonPrettyfier = new JsonPrettyfier();

        messageFragment = (MessageFragment) getSupportFragmentManager().
                findFragmentById(R.id.message_fragment);

        handleSavedInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current state.
        boolean isProgressDialogShowing = progressDialog != null && progressDialog.isShowing();
        outState.putBoolean(BUNDLE_KEY_PROGRESS_DIALOG_SHOWING, isProgressDialogShowing);

        // Cancel the fetch link tasks.
        if (fetchLinksHandler != null) {
            fetchLinksHandler.cancel();
        }

        hideProgressDialog();
    }

    public void sendMessageButtonClicked(View view) {
        KeyboardUtils.hideSoftKeyboard(this);
        getMessageResult();
    }

    @Override
    public void onLinksFetched(List<Link> links) {
        hideProgressDialog();

        // Get the input message from the fragment.
        String message =  messageFragment.getCurrentMessage();

        showResult(new MessageResult(matcher.findMentions(message),
                matcher.findEmoticons(message), links));
    }

    private void handleSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.getBoolean(BUNDLE_KEY_PROGRESS_DIALOG_SHOWING)) {
                showProgressDialog();
                getMessageResult();
            }
        }
    }

    /**
     * Gets and shows the message result from the input message. If links are provided a progress
     * dialog is shown while the HTML titles are being fetched.
     */
    private void getMessageResult() {
        String message =  messageFragment.getCurrentMessage();
        List<String> linkUrls = matcher.findLinkUrls(this, message);

        // Check if the input message contains any links.
        if (linkUrls != null && !linkUrls.isEmpty()) {
            // Show a progress dialog.
            showProgressDialog();

            // Fetch the titles.
            fetchLinksHandler = new FetchLinksHandler();
            fetchLinksHandler.fetchLinks(this, linkUrls, this);
        } else {
            // Show the result.
            showResult(new MessageResult(matcher.findMentions(message),
                    matcher.findEmoticons(message), null));
        }
    }

    /**
     * Shows the message result or an error message in the text view.
     * @param messageResult the message result to show.
     */
    private void showResult(MessageResult messageResult) {
        String jsonString = jsonPrettyfier.toReadableString(this, messageResult);

        if (jsonString != null) {
            messageFragment.setResultMessage(jsonString);
        } else {
            messageFragment.setResultMessage(getString(R.string.unable_to_serialize_message));
        }
    }

    private void showProgressDialog() {
        hideProgressDialog();

        progressDialog = ProgressDialog.show(this,
                getString(R.string.fetch_link_titles_progress_dialog_title),
                getString(R.string.fetch_link_titles_progress_dialog_message),
                true);
    }

    private void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
